const cors =  require('cors')
const bodyParser = require('body-parser')

const express = require("express");

const app = express();

app.use(express.urlencoded({
    extended: false
}));
app.use(express.json());
const {
    body,
    validationResult
} = require('express-validator');

// app.use(cors)
app.options('*', cors())
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", '*');
    res.header("Access-Control-Allow-Credentials", true);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header("Access-Control-Allow-Headers", 'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json');
    res.header("Content-Type", "application/json")
    next();
});


app.post(
    '/api/login',
    body('email').isEmail().withMessage({
        code: 123,
        message: "Invalid email"
    }),
    body('password').isLength({
        min: 6
    }).withMessage({
        code: 121,
        message: "password should be 6 character long"
    }),
    (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const error = errors.array()
            for (var err of error)
                console.log(err.msg);
            return res.status(400).json({
                success: false,
                error: err.msg
            });
        } else {
            const response = {
                email: req.body.email,
                password: req.body.password
            }
            console.log(response);
            res.status(200).send({
                success: true,
                data: response
            })
        }
    },
);

port = process.env.PORT || 3000

console.log(`Mamazone running on port : ${port}`)

app.listen(port)